const express = require("express");
const path = require("path");
const app = express();
const port = 3000;
const userDetails = require("./userDetails.json");
const viewRoutes = require("./routes/viewRoutes");

const aLoggerMiddleware = (req, res, next) => {
  console.log(req.method, req.url);
  next();
};
app.use(aLoggerMiddleware);
app.use(express.static("public"), express.static("dist"));
app.use(viewRoutes);

app.get("/api/userBalance", (req, res) => {
  res.json(userDetails.info);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
